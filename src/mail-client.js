const imaps = require('imap-simple')
const simpleParser = require("mailparser").simpleParser
const sanitizeHtml = require('sanitize-html')
const nodemailer = require("nodemailer");

export class MailClient {

    async authUser(user) {
        this.config = {
            imap: {
              user: user.login,
              password: user.password,
              host: 'imap.' + user.host,
              port: 993,
              tls: true,
              authTimeout: 5000
            }
        }
        this.user = user
        try {
            this.connection = await imaps.connect(this.config)
            return true
        } catch(err) {
            console.log(err.message)
            this.processError(err)
        }
    }

    getBoxStats() {
        return this.box.messages
    }

    async reconnect() {
        try {
            this.connection = await imaps.connect(this.config)
        } catch(err) {
            console.log("reconnect fatal error: " + err.message)
        }
    }

    async loadBoxes() {
        try {
            const boxes = await this.connection.getBoxes()
            let boxes_array = []
            for(let box in boxes) {
                if(boxes[box]) {
                    if(boxes[box].children) {
                        for(let child in boxes[box].children) {
                            boxes_array.push({
                                name: child,
                                path: box + '/' + child
                            })
                        }
                    } else {
                        boxes_array.push({
                            name: box === "INBOX" ? 'Входящие' : box,
                            path: box
                        })
                    }
                } 
            }
            return boxes_array
        } catch (err) {
            this.processError(err)
            return []
        }
    }

    async openBox(boxName) {
        try {
            this.box = await this.connection.openBox(boxName)
            let messages = await this.connection.search(['ALL'], {bodies: ['HEADER'], markSeen: false})
            let id = messages.length

            messages = messages.map(message => {
                const part = message.parts.filter(part => part.which == 'HEADER')
                const from = part[0].body.from[0]
                const index = from.lastIndexOf('<')
                const from_name = this.removeBraces(from.substring(0, index - 1))
                const from_mail = from_name ? from.substring(index + 1, from.length - 1) : from

                const date = new Date(part[0].body.date[0]);
                const month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                const today = new Date()
                let dateString = ''
                if(today.getFullYear() == date.getFullYear() && today.getMonth() == date.getMonth() && today.getDate() == date.getDate()) {
                    const minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()
                    const hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours()
                    dateString = hours + ':' + minutes
                } else {
                    dateString = date.getDate() + ' ' + month[date.getMonth()]
                }

                return {
                  id: id--, 
                  uid: message.attributes.uid,
                  subject: part[0].body.subject ? part[0].body.subject[0] : "no subject",
                  from_name: from_name ? from_name : from_mail, 
                  from_mail: from_mail,
                  date: dateString,
                  readed: message.attributes.flags.indexOf('\\Seen') > -1
                }
            })
            return messages.reverse()
        } catch(err) {
            this.processError(err)
        }
    }

    async retrieveMessage(uid) {
        try {
            let messages = await this.connection.search([['UID', uid]], { bodies: [''], markSeen: false })
            const message = messages[0]
            const parsed = await simpleParser(message.parts[0].body)
            
            const headers = {
                email: parsed.headers.get('from').value[0].address,
                from: parsed.headers.get('from').value[0].name ? parsed.headers.get('from').value[0].name : parsed.headers.get('from').value[0].address,
                subject: parsed.headers.get('subject') ? parsed.headers.get('subject') : 'no subject'
            }
            
            const attachments = parsed.attachments.filter(attachment => attachment.contentDisposition === 'attachment')

            if(parsed.html) {
                const sanitized = sanitizeHtml(parsed.html, {
                    allowedTags: false,
                    allowedAttributes: false
                }) 
                let html = ''
                if(sanitized.includes('<body')) {
                    const start = sanitized.search('<body')
                    const end = sanitized.search('</body>')
                    html = '<div' + sanitized.substring(start + '<body'.length, end) + '</div>'
                } else {
                    html = sanitized
                }
                return {
                    attachments: attachments,
                    html: html,
                    headers: headers
                }
            } else if(parsed.text) {
                return {
                    attachments: attachments,
                    text: parsed.text.split("\n").join("<br />"),
                    headers: headers
                }
            } else {
                return {
                    attachments: attachments,
                    headers: headers
                }
            }
        } catch(err) {
            this.processError(err)
        }
    }

    async sendMessage(message) {
        const transport = {
            host: 'smtp.' + this.user.host,
            port: 465,
            secure: true,
            auth: {
                user: this.user.login,
                pass: this.user.password
            }
        }
        const transporter = nodemailer.createTransport(transport)
        const message_smtp = {
            from: this.user.name + ' <' + this.user.login + '>',
            to: message.address,
            subject: message.subject,
            text: message.body,
            attachments: message.attachments.map(attachment => ({
                filename: attachment.filename,
                path: attachment.path[0]
            }))
        }
        try {
            const info = await transporter.sendMail(message_smtp)
            if(info.accepted.length > 0) {
                return {
                    result: true,
                    error: null
                }
            }
        } catch (err) {
            return {
                result: false, 
                error: err.message
            }
        }
    } 

    removeBraces(from_name) {
        if(from_name[0] == '"' && from_name[from_name.length - 1] == '"') {
          from_name = from_name.substring(1, from_name.length - 1);
        }
        return from_name
    }

    processError(error) {
        const message = error.message
        console.log(message)

        this.reconnect()
        /*
        if(message.includes('Not authenticated')) {
            this.reconnect()
        }*/
    }
}