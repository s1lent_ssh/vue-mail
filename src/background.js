'use strict'

import { dialog, app, protocol, BrowserWindow, ipcMain, Menu, MenuItem } from 'electron'
import { createProtocol, installVueDevtools } from 'vue-cli-plugin-electron-builder/lib'
import { MailClient } from './mail-client'
import { getDownloadsFolder } from 'platform-folders'

const isDevelopment = process.env.NODE_ENV !== 'production'

const client = new MailClient()
const sqlite3 = require('sqlite3').verbose()
const fs = require('fs')

dialog.showErrorBox = (title, content) => {
    console.log(title + ': ' + content)
    if(content.includes('ECONNRESET')) {
        client.reconnect()
        .then(() => {
            loadInbox()
        })
    }
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win

function onWindowLoaded() {
    let db = new sqlite3.Database('.accounts.db');
    db.serialize(() => {
  	    db.run('CREATE TABLE IF NOT EXISTS accounts (id INTEGER PRIMARY KEY, host TEXT, login TEXT, password TEXT, name TEXT)')
  	    db.get('SELECT * FROM accounts', [], async (err, user) => {
            if(err) {
                console.log(err.message)
            } else {
  	      	    if(user) {
                    await client.authUser(user)
                    loadInbox()
  	      	    } else {
                    win.webContents.send('App', 'loginMode')
                }
            }
  	    })
      db.close()
    })
}

async function loadInbox() {
    win.webContents.send('App', 'viewMessageMode')
    win.webContents.send('MessageViewer', 'showSpinner')
    const boxes = await client.loadBoxes()
    win.webContents.send('MessagesList', 'showBoxes', boxes)
    await loadBoxMessages('Inbox')
}

async function loadBoxMessages(name) {
    win.webContents.send('MessageViewer', 'showSpinner')
    win.webContents.send('MessagesList', 'clearMessages')
    const messages = await client.openBox(name)
    if(messages.length > 0) {
        win.webContents.send('MessagesList', 'showMessages', messages)
        await retrieveMessage(messages[0].uid)
    } else {
        win.webContents.send('MessagesList', 'showEmptyBox')
        win.webContents.send('MessageViewer', 'showMessage', null)
    }
}

async function retrieveMessage(uid) {
	win.webContents.send('MessageViewer', 'showSpinner')
    const message = await client.retrieveMessage(uid)
    win.webContents.send('MessageViewer', 'showMessage', message)
}

ipcMain.on('MessageListItem', async (event, message, uid) => {
    if(message === 'retrieveMessage') {
        retrieveMessage(uid)
    }
})

ipcMain.on('MessageViewer', (event, message, attachment) => {
    if(message === 'downloadFile') {
        fs.writeFile(getDownloadsFolder() + '\\' + attachment.filename, attachment.content, (err) => err ? console.log(err.message) : null)
    }
})

ipcMain.on('TopBar', (event, message) => {
    switch(message) {
        case 'logout': 
            let db = new sqlite3.Database('.accounts.db');
            db.serialize(() => {
  	            db.run('DELETE FROM accounts')
                db.close()
            })
            win.webContents.send('App', 'loginMode')
            break
        case 'stats':
            win.webContents.send('App', 'showStats', client.getBoxStats())
            break
    }
})

ipcMain.on('MessageWriter', async (event, message, argument) => {
    switch(message) {
        case 'sendMessage':
            const { result, error } = await client.sendMessage(argument)
            if(result) {
                win.webContents.send('MessageWriter', 'messageSent')
            } else {
                win.webContents.send('MessageWriter', 'messageError', error)
            }
            break
        case 'openFileDialog':
            dialog.showOpenDialog({ properties: ['openFile'] }, files => {
                if (files) win.webContents.send('MessageWriter', 'setFile', files)
            })
            break
    }
})

ipcMain.on('MessagesList', async (event, message, box) => {
    if(message === 'selectBox') {
        loadBoxMessages(box)
    }
})

ipcMain.on('LoginScreen', async (event, message, user) => {
    if(message === 'authRequest') {
	    if(await client.authUser(user)) {
            let db = new sqlite3.Database('.accounts.db')
            db.run('INSERT INTO accounts(host, login, password, name) VALUES (?, ?, ?, ?)', [user.host, user.login, user.password, user.name])
            db.close()
	    	loadInbox()
	    } else {
	    	win.webContents.send('LoginScreen', 'showAuthError', true)
        }
    }
})

// Standard scheme must be registered before the app is ready
protocol.registerStandardSchemes(['app'], { secure: true })
function createWindow () {
  // Create the browser window.
  win = new BrowserWindow({ width: 1150, height: 600, frame: false, minWidth: 1150, minHeight: 600 })

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    win.loadURL(process.env.WEBPACK_DEV_SERVER_URL)
    if (!process.env.IS_TEST) win.webContents.openDevTools()
  } else {
    createProtocol('app')
    // Load the index.html when not in development
    win.loadURL('app://./index.html')
  }

  win.on('closed', () => {
    win = null
  })

  win.webContents.once('dom-ready', onWindowLoaded);
}

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow()
  }
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => {
  if (isDevelopment && !process.env.IS_TEST) {
    // Install Vue Devtools
    try {
      await installVueDevtools()
    } catch (e) {
      console.error('Vue Devtools failed to install:', e.toString())
    }
  }
  createWindow()
})

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', data => {
      if (data === 'graceful-exit') {
        app.quit()
      }
    })
  } else {
    process.on('SIGTERM', () => {
      app.quit()
    })
  }
}
